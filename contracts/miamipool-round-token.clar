(impl-trait 'ST1PQHQKV0RJXZFY1DGX8MNSNYVE3VGZJSRTPGZGM.semi-fungible-token-trait.semi-fungible-token-trait)

(define-fungible-token miami-round-token)
(define-map token-supply {token-id: uint} uint)
(define-map owner-balance {user: principal, token-id: uint} uint)

;; Get a token type balance of the passed principal
(define-read-only (get-balance (round uint) (user principal))
    (ok (default-to u0 (map-get? owner-balance {user: user, token-id: round})))
)

;; Get the total SFT balance of the passed principal.
(define-read-only (get-overall-balance (user principal))
    (ok (ft-get-balance miami-round-token user))
)

;; Get the current total supply of a token type.
(define-read-only (get-total-supply (round uint))
    (ok (default-to u0 (map-get? token-supply {token-id: round})))
)

;; Get the overall SFT supply.
(define-read-only (get-overall-supply)
    (ok (ft-get-supply miami-round-token))
)

;; Get the number of decimal places of a token type.
(define-read-only (get-decimals (decimals uint))
    (ok u0)
)

;; Get an optional token URI that represents metadata for a specific token.
(define-read-only (get-token-uri (round uint))
    (ok none)
)

;; Transfer from one principal to another.
(define-public (transfer (round uint) (amount uint) (from principal) (to principal))
	(ok true)
)

;; Transfer from one principal to another with a memo.
(define-public (transfer-memo (round uint) (amount uint) (from principal) (to principal) (memo (buff 34)))
    (ok true)
)

;; Transfer many tokens at once.
(define-public (transfer-many (transfers (list 200 {token-id: uint, amount: uint, sender: principal, recipient: principal})))
	(ok true)
)
;; Transfer many tokens at once with memos.
(define-public (transfer-many-memo (recipients (list 200 {token-id: uint, amount: uint, sender: principal, recipient: principal, memo: (buff 34)})))
	(ok true)
)

(define-public (mint)
    (contract-call? 'SP343J7DNE122AVCSC4HEK4MF871PW470ZSXJ5K66.miamipool-v1 principal-to-id tx-sender)
    ;; (begin
    ;;     (let ((id (contract-call? 'SP343J7DNE122AVCSC4HEK4MF871PW470ZSXJ5K66.miamipool-v1 principal-to-id tx-sender)))
    ;;         (ok id)
    ;;     )
    ;; )
    
)