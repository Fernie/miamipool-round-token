
import { Clarinet, Tx, Chain, Account, types } from 'https://deno.land/x/clarinet@v0.15.4/index.ts';
import { assertEquals } from 'https://deno.land/std@0.108.0/testing/asserts.ts';

Clarinet.test({
    name: "Ensure that <...>",
    async fn(chain: Chain, accounts: Map<string, Account>) {
        let deployer = accounts.get('deployer')!
        let wallet_1 = accounts.get('wallet_1')!
        let wallet_2 = accounts.get('wallet_2')!
        let wallet_3 = accounts.get('wallet_3')!
        let wallet_4 = accounts.get('wallet_4')!
        let wallet_5 = accounts.get('wallet_5')!
        let wallet_6 = accounts.get('wallet_6')!
        let wallet_7 = accounts.get('wallet_7')!
        let wallet_8 = accounts.get('wallet_8')!
        let wallet_9 = accounts.get('wallet_9')!
        let wallet_10 = accounts.get('wallet_10')!
        let wallet_11 = accounts.get('wallet_11')!
        let wallet_12 = accounts.get('wallet_12')!
        let wallet_13 = accounts.get('wallet_13')!
        let wallet_14 = accounts.get('wallet_14')!
        let wallet_15 = accounts.get('wallet_15')!
        let wallet_16 = accounts.get('wallet_16')!
        let wallet_17 = accounts.get('wallet_17')!
        let wallet_18 = accounts.get('wallet_18')!
        let wallet_19 = accounts.get('wallet_19')!
        let wallet_20 = accounts.get('wallet_20')!

        let block = chain.mineBlock([
            Tx.contractCall(
                'SP466FNC0P7JWTNM2R9T199QRZN1MYEDTAR0KP27.miamicoin-auth',
                'initialize-contracts',
                [
                    types.principal(
                        'SP466FNC0P7JWTNM2R9T199QRZN1MYEDTAR0KP27.miamicoin-core-v1'
                    ),
                ],
                "SP466FNC0P7JWTNM2R9T199QRZN1MYEDTAR0KP27"
            ),
            Tx.contractCall(
                'SP343J7DNE122AVCSC4HEK4MF871PW470ZSXJ5K66.miamipool-v1',
                'add-funds',
                [types.uint(1000000)],
                wallet_1.address
            ),
            Tx.contractCall(
                'SP343J7DNE122AVCSC4HEK4MF871PW470ZSXJ5K66.miamipool-v1',
                'add-funds',
                [types.uint(2000000)],
                wallet_2.address
            ),
            Tx.contractCall(
                'SP343J7DNE122AVCSC4HEK4MF871PW470ZSXJ5K66.miamipool-v1',
                'add-funds',
                [types.uint(3000000)],
                wallet_3.address
            ),
            Tx.contractCall(
                'SP343J7DNE122AVCSC4HEK4MF871PW470ZSXJ5K66.miamipool-v1',
                'add-funds',
                [types.uint(4000000)],
                wallet_4.address
            ),
            Tx.contractCall(
                'SP343J7DNE122AVCSC4HEK4MF871PW470ZSXJ5K66.miamipool-v1',
                'add-funds',
                [types.uint(5000000)],
                wallet_5.address
            ),
            Tx.contractCall(
                'SP343J7DNE122AVCSC4HEK4MF871PW470ZSXJ5K66.miamipool-v1',
                'add-funds',
                [types.uint(6000000)],
                wallet_6.address
            ),
            Tx.contractCall(
                'SP343J7DNE122AVCSC4HEK4MF871PW470ZSXJ5K66.miamipool-v1',
                'add-funds',
                [types.uint(7000000)],
                wallet_7.address
            ),
            Tx.contractCall(
                'SP343J7DNE122AVCSC4HEK4MF871PW470ZSXJ5K66.miamipool-v1',
                'add-funds',
                [types.uint(8000000)],
                wallet_8.address
            ),
            Tx.contractCall(
                'SP343J7DNE122AVCSC4HEK4MF871PW470ZSXJ5K66.miamipool-v1',
                'add-funds',
                [types.uint(9000000)],
                wallet_9.address
            ),
        ])
        chain.mineBlock([
            Tx.contractCall(
                'SP466FNC0P7JWTNM2R9T199QRZN1MYEDTAR0KP27.miamicoin-core-v1',
                'register-user',
                [types.none()],
                wallet_1.address
            ),
            Tx.contractCall(
                'SP466FNC0P7JWTNM2R9T199QRZN1MYEDTAR0KP27.miamicoin-core-v1',
                'register-user',
                [types.none()],
                wallet_2.address
            ),
            Tx.contractCall(
                'SP466FNC0P7JWTNM2R9T199QRZN1MYEDTAR0KP27.miamicoin-core-v1',
                'register-user',
                [types.none()],
                wallet_3.address
            ),
            Tx.contractCall(
                'SP466FNC0P7JWTNM2R9T199QRZN1MYEDTAR0KP27.miamicoin-core-v1',
                'register-user',
                [types.none()],
                wallet_4.address
            ),
            Tx.contractCall(
                'SP466FNC0P7JWTNM2R9T199QRZN1MYEDTAR0KP27.miamicoin-core-v1',
                'register-user',
                [types.none()],
                wallet_5.address
            ),
            Tx.contractCall(
                'SP466FNC0P7JWTNM2R9T199QRZN1MYEDTAR0KP27.miamicoin-core-v1',
                'register-user',
                [types.none()],
                wallet_6.address
            ),
            Tx.contractCall(
                'SP466FNC0P7JWTNM2R9T199QRZN1MYEDTAR0KP27.miamicoin-core-v1',
                'register-user',
                [types.none()],
                wallet_7.address
            ),
            Tx.contractCall(
                'SP466FNC0P7JWTNM2R9T199QRZN1MYEDTAR0KP27.miamicoin-core-v1',
                'register-user',
                [types.none()],
                wallet_8.address
            ),
            Tx.contractCall(
                'SP466FNC0P7JWTNM2R9T199QRZN1MYEDTAR0KP27.miamicoin-core-v1',
                'register-user',
                [types.none()],
                wallet_9.address
            ),
            Tx.contractCall(
                'SP466FNC0P7JWTNM2R9T199QRZN1MYEDTAR0KP27.miamicoin-core-v1',
                'register-user',
                [types.none()],
                wallet_10.address
            ),
            Tx.contractCall(
                'SP466FNC0P7JWTNM2R9T199QRZN1MYEDTAR0KP27.miamicoin-core-v1',
                'register-user',
                [types.none()],
                wallet_11.address
            ),
            Tx.contractCall(
                'SP466FNC0P7JWTNM2R9T199QRZN1MYEDTAR0KP27.miamicoin-core-v1',
                'register-user',
                [types.none()],
                wallet_12.address
            ),
            Tx.contractCall(
                'SP466FNC0P7JWTNM2R9T199QRZN1MYEDTAR0KP27.miamicoin-core-v1',
                'register-user',
                [types.none()],
                wallet_13.address
            ),
            Tx.contractCall(
                'SP466FNC0P7JWTNM2R9T199QRZN1MYEDTAR0KP27.miamicoin-core-v1',
                'register-user',
                [types.none()],
                wallet_14.address
            ),
            Tx.contractCall(
                'SP466FNC0P7JWTNM2R9T199QRZN1MYEDTAR0KP27.miamicoin-core-v1',
                'register-user',
                [types.none()],
                wallet_15.address
            ),
            Tx.contractCall(
                'SP466FNC0P7JWTNM2R9T199QRZN1MYEDTAR0KP27.miamicoin-core-v1',
                'register-user',
                [types.none()],
                wallet_16.address
            ),
            Tx.contractCall(
                'SP466FNC0P7JWTNM2R9T199QRZN1MYEDTAR0KP27.miamicoin-core-v1',
                'register-user',
                [types.none()],
                wallet_17.address
            ),
            Tx.contractCall(
                'SP466FNC0P7JWTNM2R9T199QRZN1MYEDTAR0KP27.miamicoin-core-v1',
                'register-user',
                [types.none()],
                wallet_18.address
            ),
            Tx.contractCall(
                'SP466FNC0P7JWTNM2R9T199QRZN1MYEDTAR0KP27.miamicoin-core-v1',
                'register-user',
                [types.none()],
                wallet_19.address
            ),
            Tx.contractCall(
                'SP466FNC0P7JWTNM2R9T199QRZN1MYEDTAR0KP27.miamicoin-core-v1',
                'register-user',
                [types.none()],
                wallet_20.address
            ),
            Tx.contractCall(
                'SP466FNC0P7JWTNM2R9T199QRZN1MYEDTAR0KP27.miamicoin-core-v1',
                'register-user',
                [types.none()],
                deployer.address
            ),
        ])
        // console.log(block.receipts)
        // skip 150 blocks
        for (let i = 0; i < 150; i++) {
            chain.mineBlock([])
        }

        chain.mineEmptyBlockUntil(1000);

        block = chain.mineBlock([
            Tx.contractCall('SP466FNC0P7JWTNM2R9T199QRZN1MYEDTAR0KP27.miamicoin-core-v1', 'get-registered-users-nonce', [], wallet_9.address),
            Tx.contractCall('SP343J7DNE122AVCSC4HEK4MF871PW470ZSXJ5K66.miamipool-v1', 'mine', [types.uint(1)], wallet_9.address),
        ]);



        // skip 100 blocks
        for (let i = 0; i < 100; i++) {
            chain.mineBlock([])
        }


        for (let i = 0; i < 150; i++) {
            chain.mineBlock([
                Tx.contractCall(
                    'SP343J7DNE122AVCSC4HEK4MF871PW470ZSXJ5K66.miamipool-v1',
                    'claim-mining-reward',
                    [types.uint(1)],
                    wallet_9.address
                ),
            ])
        }

        block = chain.mineBlock([
            Tx.contractCall(
                'SP343J7DNE122AVCSC4HEK4MF871PW470ZSXJ5K66.miamipool-v1',
                'payout-mia',
                [types.uint(1)],
                wallet_9.address
            ),
            Tx.contractCall(
                'SP343J7DNE122AVCSC4HEK4MF871PW470ZSXJ5K66.miamipool-v1',
                'get-round',
                [types.uint(1)],
                wallet_9.address
            ),
            Tx.contractCall(
                'SP343J7DNE122AVCSC4HEK4MF871PW470ZSXJ5K66.miamipool-v1',
                'get-round-status',
                [types.uint(1)],
                wallet_9.address
            ),
        ])

        console.log(block.receipts);
        let assetMaps = chain.getAssetsMaps();
        console.log(assetMaps);
        // block.receipts[0].result.expectOk().expectBool(true)
        // block.receipts[1].result.expectOk().expectBool(true)

        // assertEquals(
        //     block.receipts[2].result,
        //     '(ok {blockHeight: u1, blocksWon: [u152], participantIds: [u1, u2, u3, u4, u5, u6, u7, u8, u9], totalMiaWon: u250000, totalStx: u45000000})'
        // )
        // assertEquals(
        //     block.receipts[3].result,
        //     '(ok {hasMined: true, lastBlockToCheck: u301, nextBlockToCheck: u153, requiredPayouts: u1, sendManyIds: [u1, u2, u3, u4, u5, u6, u7, u8, u9]})'
        // )

        // block.receipts[4].result.expectOk().expectUint(4)
        // let deployerWallet = accounts.get("deployer") as Account;
        // let wallet_1 = accounts.get('wallet_1')!;

        // let block = chain.mineBlock([
        //     Tx.contractCall(
        //         `miamipool-round-token`,
        //         "mint",
        //         [],
        //         wallet_1.address,
        //     ),
        // ]);
        // assertEquals(block.receipts.length, 1);
        // console.log(block.receipts[0].result);
        // assertEquals(block.height, 2);

    },
});
